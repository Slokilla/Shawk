<?php

namespace App\Controller;

use App\Entity\Bar;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\FavoriteController;

class SearchResultController extends AbstractController
{
    /**
     * @Route("/search/result", name="search_result")
     */
    public function index()
    {
        $bar_name = isset($_GET['bar_name']) ? $_GET['bar_name'] : '';

        $bars = $this->getDoctrine()->getRepository(Bar::class)->createQueryBuilder('o')
            ->where('LOWER(o.tradename) LIKE LOWER(:name)')
            ->setParameter('name', '%' . $bar_name . '%')
            ->getQuery()
            ->getResult();
        if ($bar_name == '') {
            $bars = $this->getDoctrine()->getRepository(Bar::class)->findAll();
        }

        return $this->render('search_result/search_result.html.twig', [
            'controller_name' => 'SearchResultController',
            'text' => $bar_name,
            'bars' => $bars,
        ]);
    }
}
