<?php

namespace App\Controller;

use App\Entity\Bar;
use App\Entity\Favorite;
use App\Entity\File;
use App\Entity\User;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\Asset\VersionStrategy\JsonManifestVersionStrategy;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Flex\Response;
use Twig\Environment;

class FavoriteController extends AbstractController
{
    /**
     * @Route("/favorite", name="favorite")
     */
    public function index()
    {
        return $this->redirectToRoute("Index");
    }

    /**
     * @Route("/favorite/{id}", name="usersFavorite", methods={"GET"})
     */
    public function findByUsers(Request $request, int $id){
        if($id){

            $favorites =  $this->getDoctrine()->getRepository(Favorite::class)->findBy(["fkUserId"=>(string)$id]);
            $package = new Package(new JsonManifestVersionStrategy($this->getParameter('kernel.project_dir').'/public/build/manifest.json'));
            $bars = array();

            foreach($favorites as $favorite){
                $fav = $this->getDoctrine()->getRepository(Bar::class)->find($favorite->getFkBarId());

                $fav->setFkOwner(null);

                if($fav->getFkImageLogo()){
                    $plainPath=$this->getDoctrine()->getRepository(File::class)->find($fav->getFkImageLogo())->getFilepath();
                    $temp = ("build/images/" . $plainPath);
                    $fav->setLogoPath($package->getUrl($temp));
                }


                array_push($bars, $fav);
            }


            $serializedResponse = $this->container->get('serializer')->serialize($bars,'json');



            echo $serializedResponse;
            return new \Symfony\Component\HttpFoundation\Response("");
        }

        return new \Symfony\Component\HttpFoundation\Response("Error");
    }

    /**
     * @Route("/favorite/bar/{id}", name="favoritesByBar", methods={"GET"})
     */
    public function findByBars(Request $request, int $id){
        if($id){
            $favorites =  $this->getDoctrine()->getRepository(Favorite::class)->findBy(["fkBarId"=>(string)$id]);
            $serializedResponse = $this->container->get('serializer')->serialize($favorites,'json');
            echo $serializedResponse;
            return new \Symfony\Component\HttpFoundation\Response("");
        }

        return new \Symfony\Component\HttpFoundation\Response("Error");
    }

    /**
     * @Route("/favorite/{userId}/{barId}", name="addFavoriteToUser", methods={"POST"})
     */
    public function addFavoriteToUser(int $userId, int $barId){
        $manager = $this->getDoctrine()->getManager();
        $entry = new Favorite();

        $entry->setFkBarId($barId);
        $entry->setFkUserId($userId);

        try{
        $manager->persist($entry);
        $manager->flush();
        }catch (Exception $exception){
            return $exception->getMessage();
        }
        return new \Symfony\Component\HttpFoundation\Response("Ok");
    }

    /**
     * @Route("/favorite/{userId}/{barId}", name="deleteFavorite", methods={"DELETE"})
     */
    public function deleteFavoriteToUser(int $userId, int $barId){
        $criteria = array_filter(array(
            'fkUserId' => $userId,
            'fkBarId' => $barId
        ));

        $toDelete = $this->getDoctrine()->getRepository(Favorite::class)->findBy($criteria);
        dump($toDelete);
        $this->getDoctrine()->getManager()->remove($toDelete[0]);
        $this->getDoctrine()->getManager()->flush();
        return new \Symfony\Component\HttpFoundation\Response("Ok");
    }
}
