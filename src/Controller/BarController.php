<?php

namespace App\Controller;

use App\Entity\Bar;
use App\Entity\Event;
use App\Entity\Favorite;
use App\Entity\File;
use App\Entity\Owner;
use App\Form\Bar1Type;
use App\Controller\FileController;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bar")
 */
class BarController extends AbstractController
{
    /**
     * @Route("/", name="bar_index", methods={"GET"})
     */
    public function index(): Response
    {
        if ($this->getUser()){
            // check if not admin or owner
            if (!array_intersect($this->getUser()->getRoles(), ['admin', 'Owner'])){
                return $this->redirectToRoute('search_result');
            }
        } else {
            return $this->redirectToRoute('app_login');
        }
        $bars = $this->getDoctrine()
            ->getRepository(Bar::class)
            ->findBy(['fkOwner' => $this->getUser()->getId()]);

        return $this->render('bar/index.html.twig', [
            'title' => 'Vos bars',
            'bars' => $bars,
        ]);
    }

    /**
     * @Route("/bars/json", name="bars_json", methods={"GET"})
     */
    public function barsInJson(): Response
    {
        $bars = $this->getDoctrine()
            ->getRepository(Bar::class)
            ->findAll();

        $bar_names = [];

        foreach ($bars as $bar){
            $bar_names[$bar->getTradename()] =  null;
        }

        return JsonResponse::create($bar_names);
    }

    /**
     * @Route("/new", name="bar_new", methods={"GET","POST"})
     */
    public function new(Request $request, LoggerInterface $logger): Response
    {
        $bar = new Bar();
        $form = $this->createForm(Bar1Type::class, $bar);
        $form->handleRequest($request);

        //Gestion de la photo de profil
        if ($form->isSubmitted() && $form->isValid()) {

            $Owner = $this->getDoctrine()->getRepository(Owner::class)->findOneBy(["fkIdUser" => $this->getUser()->getId()]);
            $bar->setFkOwner($Owner);

            $logo = $form->get('fkImageLogo')->getData();
            $fkLogo = new File();

            if($logo){

                $originalFilename = pathinfo($logo->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$logo->guessExtension();

                try {
                    $logo->move(
                        $this->getParameter('picturesDirectory'),
                        $newFilename
                    );

                    // on rajoute le fichier en base
                    $fkLogo->setFilepath($newFilename);
                    $fkLogo->setFiletype("logo");

                    $fileManager = $this->getDoctrine()->getManagerForClass(File::class);
                    $fileManager->persist($fkLogo);
                    $fileManager->flush();

                    $bar->setFkImageLogo($fkLogo);
                } catch (FileException $e) {

                    error_log("=====================" . $e->getMessage() . "================");
                }
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($bar);
            $entityManager->flush();

            return $this->redirectToRoute('bar_index');
        }

        return $this->render('bar/new.html.twig', [
            'bar' => $bar,
            'form' => $form->createView(),
            'title' => "Nouveau Bar"
        ]);
    }

    /**
     * @Route("/{id}", name="bar_show", methods={"GET"})
     */
    public function show(Bar $bar): Response
    {
        $favorite = $this->getDoctrine()
            ->getRepository(Favorite::class)
            ->findBy([
                'fkUserId'=> $this->getUser()->getId(),
                'fkBarId' => $bar->getID()]);
        $is_favorite = isset($favorite[0]) ? 1 : 0;
        $events = $this->getDoctrine()
            ->getRepository(Event::class)
            ->findByFkBar($bar->getID());
        if($bar->getFkImageLogo())
            $logo = $this->getDoctrine()->getRepository(File::class)->find($bar->getFkImageLogo())->getFilepath();
        else
            $logo = null;
        return $this->render('bar/show.html.twig', [
            'is_favorite' => $is_favorite,
            'bar' => $bar,
            'events' => $events,
            'logoPath' => $logo
        ]);
    }

    /**
     * @Route("/get/{id}", name="getBarById", methods={"GET"})
     */
    public function findById(string $id)
    {
        if($id){
            $bar =  $this->getDoctrine()->getRepository(Bar::class)->findBy(["id"=>(string)$id]);
            $serializedResponse = $this->container->get('serializer')->serialize($bar,'json');
            echo $serializedResponse;
            return new Response("");
        }

        return new Response("Error");
    }

    /**
     * @Route("/get", name="getAllBars", methods={"GET"})
     */
    public function findAll()
    {
        $bar =  $this->getDoctrine()->getRepository(Bar::class)->findAll();
        $serializedResponse = $this->container->get('serializer')->serialize($bar,'json');
        echo $serializedResponse;
    }

    /**
     * @Route("/{id}/edit", name="bar_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Bar $bar): Response
    {
        $form = $this->createForm(Bar1Type::class, $bar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $logo = $form->get('fkImageLogo')->getData();
            $fkLogo = new File();

            if($logo){

                $originalFilename = pathinfo($logo->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$logo->guessExtension();

                try {
                    $logo->move(
                        $this->getParameter('picturesDirectory'),
                        $newFilename
                    );

                    // on rajoute le fichier en base
                    $fkLogo->setFilepath($newFilename);
                    $fkLogo->setFiletype("logo");

                    $fileManager = $this->getDoctrine()->getManagerForClass(File::class);
                    $fileManager->persist($fkLogo);
                    $fileManager->flush();

                    $bar->setFkImageLogo($fkLogo);
                } catch (FileException $e) {
                    error_log("=====================" . $e->getMessage() . "================");
                }

                $this->getDoctrine()->getManager()->flush();
                return $this->redirectToRoute('bar_index');
            }
        }

        return $this->render('bar/edit.html.twig', [
            'title' => 'Édition de bar',
            'bar' => $bar,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/url_map", name="bar_url_map_add", methods={"POST"})
     */
    public function add_url_map(Request $request, Bar $bar): Response
    {
        if (!isset($_POST) || !isset($_POST['new_url'])){
            return JsonResponse::fromJsonString('{ "ko": "missing mandatory parameter" }');
        }
        $bar->setUrlMap($_POST['new_url']);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return JsonResponse::fromJsonString('{ "status": "added url" }');
    }

    /**
     * @Route("/{id}", name="bar_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Bar $bar): Response
    {
        if ($this->isCsrfTokenValid('delete'.$bar->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($bar);
            $entityManager->flush();
        }

        return $this->redirectToRoute('bar_index');
    }
}
