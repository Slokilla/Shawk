<?php

namespace App\Controller;

use App\Entity\Bar;
use App\Entity\Event;
use App\Entity\File;
use App\Form\EventType;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @Route("/event")
 */
class EventController extends AbstractController
{
    /**
     * @Route("/", name="event_index", methods={"GET"})
     */
    public function index(): Response
    {
        $events = $this->getDoctrine()
            ->getRepository(Event::class)
            ->findAll();

        if($this->getUser()){
            if(!array_intersect($this->getUser()->getRoles(), ["admin"])){
                return $this->redirectToRoute("Index");
            }
        }

        /////////////////////// GESTION DROITS /////////////////////
        if($this->getUser()){
            if(!array_intersect($this->getUser()->getRoles(), ["admin"])){
                return $this->redirectToRoute("Index");
            }
        }
        else{
            return $this->redirectToRoute("Index");
        }
        //////////////////////////////////////////////////////////

        return $this->render('event/index.html.twig', [
            'events' => $events,
        ]);
    }

    /**
     * @Route("/bar/{id}", name="events_by_bar", methods={"GET"})
     */
    public function events_by_bar(int $id): Response
    {
        $events = $this->getDoctrine()
            ->getRepository(Event::class)
            ->findByFkBar($id);

        $bar = $this->getDoctrine()
            ->getRepository(Bar::class)
            ->Find($id);

        if($this->getUser()){
            if(!array_intersect($this->getUser()->getRoles(), ["admin", "Owner"]) || !($this->getUser()->getId() == $bar->getFkOwner()->getFkIdUser()->getId()) ){
                return $this->redirectToRoute("Index");
            }
        }
        else{
            return $this->redirectToRoute("Index");
        }

        return $this->render('event/index_by_bar.html.twig', [
            'events' => $events,
            'bar' => $bar
        ]);
    }

    /**
     * @Route("/{id}/new", name="event_new", methods={"GET","POST"})
     */
    public function new(Request $request, int $id): Response
    {

        $event = new Event();
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $bar = $this->getDoctrine()->getRepository(Bar::class)->find($id);

            $event->setFkBar($bar->getID());
            $event->setPublicationDate();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('event_index');
        }

        /////////////////////// GESTION DROITS /////////////////////
        $bar = $this->getDoctrine()
            ->getRepository(Bar::class)
            ->Find($id);

        if(!$bar) return $this->redirectToRoute("Index");

        if($this->getUser()){
            if(!array_intersect($this->getUser()->getRoles(), ["admin", "Owner"]) || !($this->getUser()->getId() == $bar->getFkOwner()->getFkIdUser()->getId()) ){
                return $this->redirectToRoute("Index");
            }
        }
        else{
            return $this->redirectToRoute("Index");
        }
        //////////////////////////////////////////////////////////


        return $this->render('event/new.html.twig', [
            'event' => $event,
            'form' => $form->createView(),
            'bar_id' => $id,
        ]);
    }

    /**
     * @Route("/{id}", name="event_show", methods={"GET"})
     */
    public function show(Event $event, int $id): Response
    {
        if($event->getFkImageAffiche())
            $affiche = $this->getDoctrine()
                ->getRepository(File::class)
                ->find($event->getFkImageAffiche())
                ->getFilepath();
        else
            $affiche = null;
        return $this->render('event/show.html.twig', [
            'event' => $event,
            'affichePath' => $affiche
        ]);
    }

    /**
     * @Route("/{id}/edit", name="event_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Event $event, int $id): Response
    {
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logo = $form->get('fkImageAffiche')->getData();
            $fkLogo = new File();

            if($logo) {

                $originalFilename = pathinfo($logo->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $logo->guessExtension();

                try {
                    $logo->move(
                        $this->getParameter('picturesDirectory'),
                        $newFilename
                    );

                    // on rajoute le fichier en base
                    $fkLogo->setFilepath($newFilename);
                    $fkLogo->setFiletype("affiche");

                    $fileManager = $this->getDoctrine()->getManagerForClass(File::class);
                    $fileManager->persist($fkLogo);
                    $fileManager->flush();

                    $event->setFkImageAffiche($fkLogo);
                } catch (FileException $e) {
                    error_log("=====================" . $e->getMessage() . "================");
                }
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('event_index');
        }

        /////////////////////// GESTION DROITS /////////////////////
        $bar = $this->getDoctrine()
            ->getRepository(Bar::class)
            ->Find($id);

        if(!$bar) return $this->redirectToRoute("Index");

        if($this->getUser()){
            if(!array_intersect($this->getUser()->getRoles(), ["admin", "Owner"]) || !($this->getUser()->getId() == $bar->getFkOwner()->getFkIdUser()->getId()) ){
                return $this->redirectToRoute("Index");
            }
        }
        else{
            return $this->redirectToRoute("Index");
        }
        //////////////////////////////////////////////////////////

        return $this->render('event/edit.html.twig', [
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="event_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Event $event): Response
    {
        if ($this->isCsrfTokenValid('delete'.$event->getID(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($event);
            $entityManager->flush();
        }

        return $this->redirectToRoute('event_index');
    }

    /**
     * @Route("/bar/{id}", name="show_bar_event", methods={"GET"})
     * @return Event[]
     */

    public function showBarsEvents(Event $event)
    {
        return $this->getDoctrine()->getRepository(Event::class)->findBy(array('id' => $this->getParameter("id")));
    }
}
