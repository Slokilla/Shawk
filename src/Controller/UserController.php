<?php

namespace App\Controller;

use App\Entity\AccountType;
use App\Entity\File;
use App\Entity\User;
use App\Form\UserType;
use mysql_xdevapi\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function listUsers()
    {
        $usrs = $this->getDoctrine()->getRepository(User::class)->findAll();
        return $this->render('Users/listUsers.html.twig', [
            'controller_name' => 'UserController',
            'users' => $usrs,
            'name' => "Utilisateurs"
        ]);
    }

    /**
     * @Route("/user/create", name="createUser")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createUser()
    {

        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        $double = false;

        if (isset($_POST)){
            foreach($users as $user){
                if($user->getEmail() == $_POST['email'])
                    $double = true;
            }
        }
        else{
            if (!isset($_POST['email'])){
                return $this->redirectToRoute('app_login', ['email' => 'empty']);
            }
            if (!isset($_POST['password'])){
                return $this->redirectToRoute('app_login', ['password' => 'empty']);
            }
            if (!isset($_POST['confirm_password'])){
                return $this->redirectToRoute('app_login', ['confirm_password' => 'empty']);
            }
            if ($_POST['password'] != $_POST['confirm_password']){
                return $this->redirectToRoute('app_login', ['password' => 'mismatch']);
            }
            if ($double)
                return $this->redirectToRoute('app_login', ['post' => 'no']);

            return $this->redirectToRoute('app_login', ['post' => 'no']);
        }


        $user = new User();
        $user->setEmail($_POST['email']);
        $user->setFirstName($_POST['first_name']);
        $user->setLastName($_POST['last_name']);
        $user->setPass(password_hash($_POST['password'], PASSWORD_DEFAULT));
        $user->setIsActive(1); //enable or disable
        $user->setSignupDate(time());

        $accountType = $this->getDoctrine()->getRepository(AccountType::class)->find(2);
        $user->setFkAccountType($accountType);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();



        return $this->render('Users/createUserValidation.html.twig', [
            'controller_name' => 'UserController',
            'user' => 'created'
        ]);
    }



    /**
     * @Route("/user/{id}", name="readUser")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function readUsers(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $logo = $form->get('fkProfilPicture')->getData();
            $fkProfilPic = new File();
            if($logo){

                $originalFilename = pathinfo($logo->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$logo->guessExtension();

                try {
                    $logo->move(
                        $this->getParameter('picturesDirectory'),
                        $newFilename
                    );

                    // on rajoute le fichier en base
                    $fkProfilPic->setFilepath($newFilename);
                    $fkProfilPic->setFiletype("profil_picture");

                    $fileManager = $this->getDoctrine()->getManagerForClass(File::class);
                    $fileManager->persist($fkProfilPic);
                    $fileManager->flush();

                    $user->setFkProfilPicture($fkProfilPic);
                } catch (FileException $e) {
                    error_log("=====================" . $e->getMessage() . "================");
                }
            }
            $this->getDoctrine()->getManager()->flush();
        }

        if($user->getFkProfilPicture())
            $profilPic = $this->getDoctrine()->getRepository(File::class)->find($user->getFkProfilPicture())->getFilepath();
        else
            $profilPic = null;

        return $this->render('Users/showUser.html.twig', [
            'controller_name' => 'UserController',
            'profilPic' => $profilPic,
            'user' => $user,
            'form' => $form->createView(),
            'name' => "Mon compte"
        ]);
    }


    public function updateUser(User $user)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->getRepository(User::class)->find($user->getId())->setAll($user);
            $entityManager->flush();

        }catch (Exception $e){
            return $e->getMessage();
        }
        return "updated";
    }

    /**
     * @Route("/user/{id}/delete", name="deleteUser")
     * @param int $id
     * @return String
     */
    public function deleteUser(int $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $toDelete = $entityManager->getRepository(User::class)->find($id);
        $entityManager->remove($toDelete);
        $entityManager->flush();


        return "deleted";
    }

}
