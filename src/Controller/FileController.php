<?php

namespace App\Controller;

use App\Entity\File;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Routing\Annotation\Route;

class FileController extends AbstractController
{

    public function addFileToBase(\Symfony\Component\HttpFoundation\Request $request,File $file):File
    {
        if($file->getFilepath() && $file->getFiletype()){
            $entityManager =$this->getDoctrine()->getManager();
            $entityManager->persist($file);
            $entityManager->flush();

            error_log("============ Dans addFile ===========");
            return $file;
        }
        throw new Exception("The File has not been uploaded");
    }
}
