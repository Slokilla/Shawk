<?php

namespace App\Controller;

use App\Entity\Owner;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class OwnerController extends AbstractController
{
    public function getOwner(int $id):Owner
    {
        return $this->getDoctrine()->getRepository(Owner::class)->findOneBy(["fkIdUser" => $id]);
    }
}
