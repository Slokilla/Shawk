<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class mainController extends AbstractController{
    /**
     * @Route("/", name="Index", methods={"GET"})
     */
    public function index(Environment $twig){
        if($this->getUser()){
            if (array_intersect($this->getUser()->getRoles(), ['Owner'])){
                try {
                    return $this->redirectToRoute("bar_index");
                } catch (LoaderError $e) {
                    echo($e->getMessage());
                } catch (RuntimeError $e) {
                    echo($e->getMessage());
                } catch (SyntaxError $e) {
                    echo($e->getMessage());
                }
            }
        }

        try {
            $content = $twig->render('Static/index.html.twig', ['name' => 'index']);
            return new Response($content);
        } catch (LoaderError $e) {
            echo($e->getMessage());
        } catch (RuntimeError $e) {
            echo($e->getMessage());
        } catch (SyntaxError $e) {
            echo($e->getMessage());
        }

        return new Response("Error");
    }


    public function notFound(Environment $twig)
    {
        $content = $twig ->render('Static/error.html.twig', ['name' => 'erreur']);
        return new Response($content);
    }

    public function uploadImage(Environment $twig)
    {
        $content = $twig ->render('Static/index.html.twig');
        return new Response($content);
    }

    /**
     * @Route("/contact", name="Contact", methods={"GET"})
     */
    public function contact(Environment $twig)
    {
        $content = $twig ->render('Static/contact.html.twig', ['name' => 'Contact']);
        return new Response($content);
    }

    /**
     * @Route("/cgu", name="CGU", methods={"GET"})
     */
    public function cgu(Environment $twig)
    {
        $content = $twig ->render('Static/cgu.html.twig', ['name' => 'Conditions']);
        return new Response($content);
    }

}