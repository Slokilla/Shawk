<?php

namespace App\Form;

use App\Entity\Bar;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tradename')
            ->add('address')
            ->add('zipCode')
            ->add('city')
            ->add('urlMap')
            ->add('information')
            ->add('approved')
            ->add('active')
            ->add('fkImageLogo')
            ->add('fkFileKbis')
            ->add('fkOwner')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bar::class,
        ]);
    }
}
