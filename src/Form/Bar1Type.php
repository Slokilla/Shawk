<?php

namespace App\Form;

use App\Entity\Bar;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class Bar1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tradename')
            ->add('address')
            ->add('zipCode')
            ->add('city')
            ->add('information', TextareaType::class, ["attr" => ['class' =>  "materialize-textarea"]])
            ->add('fkImageLogo', FileType::class, [
                                                        'constraints' => [
                                                            new File([
                                                                'maxSize' => '20000k',
                                                                'mimeTypes' => [
                                                                    'image/*',
                                                                ],
                                                                'mimeTypesMessage' => 'Veuillez selectionner une image',
                                                            ])
                                                        ],
                                                        'attr'=>['accept' => "image/*"],
                                                        "required" => false,
                                                        "mapped"=> false,
                                                        'data_class' => File::class,
                                                            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bar::class,
        ]);
    }
}
