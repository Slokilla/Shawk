<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Bar
 *
 * @ORM\Table(name="Bar", uniqueConstraints={@ORM\UniqueConstraint(name="bar_ID_uindex", columns={"ID"})}, indexes={@ORM\Index(name="Bar_File_ID_fk", columns={"fk_file_kbis"}), @ORM\Index(name="Bar_File_logo_fk_", columns={"fk_image_logo"}), @ORM\Index(name="bar_Owner_fk_id_user_fk", columns={"fk_owner_id"})})
 * @ORM\Entity
 */
class Bar
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Bar")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tradename", type="string", length=255, nullable=false)
     */
    private $tradename;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * @var string|null
     *
     * @ORM\Column(name="zip_code", type="string", length=255, nullable=true)
     */
    private $zipCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url_map", type="string", length=255, nullable=true)
     */
    private $urlMap;

    /**
     * @var string|null
     *
     * @ORM\Column(name="information", type="string", length=500, nullable=true)
     */
    private $information;

    /**
     * @var bool
     *
     * @ORM\Column(name="approved", type="boolean", nullable=false)
     */
    private $approved = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $active = '1';

    /**
     * @var int|null
     *
     * @ORM\Column(name="fk_image_logo", type="integer", nullable=true)
     */
    private $fkImageLogo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="fk_file_kbis", type="integer", nullable=true)
     */
    private $fkFileKbis;

    /**
     * @var Owner
     *
     * @ORM\ManyToOne(targetEntity="Owner")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_owner_id", referencedColumnName="fk_id_user")
     * })
     */
    private $fkOwner;

    /**
     * @var string
     */
    private $logoPath;

    public function getID(): ?int
    {
        return $this->id;
    }

    public function getTradename(): ?string
    {
        return $this->tradename;
    }

    public function setTradename(string $tradename): self
    {
        $this->tradename = $tradename;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getUrlMap(): ?string
    {
        return $this->urlMap;
    }

    public function setUrlMap(?string $urlMap): self
    {
        $this->urlMap = $urlMap;

        return $this;
    }

    public function getInformation(): ?string
    {
        return $this->information;
    }

    public function setInformation(?string $information): self
    {
        $this->information = $information;

        return $this;
    }

    public function getApproved(): ?bool
    {
        return $this->approved;
    }

    public function setApproved(bool $approved): self
    {
        $this->approved = $approved;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getFkImageLogo(): ?int
    {
        return $this->fkImageLogo;
    }

    public function setFkImageLogo(File $fkImageLogo): self
    {
        $this->fkImageLogo = $fkImageLogo->getId();

        return $this;
    }

    public function getFkFileKbis(): ?int
    {
        return $this->fkFileKbis;
    }

    public function setFkFileKbis(?int $fkFileKbis): self
    {
        $this->fkFileKbis = $fkFileKbis;

        return $this;
    }

    public function getFkOwner(): ?Owner
    {
        return $this->fkOwner;
    }

    public function setFkOwner(?Owner $fkOwner): self
    {
        $this->fkOwner = $fkOwner;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogoPath(): ?string
    {
        return $this->logoPath;
    }

    /**
     * @param string $logoPath
     */
    public function setLogoPath(string $logoPath): self
    {
        $this->logoPath = $logoPath;

        return $this;
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        return $this->getTradename();
    }
}
