<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Favorite
 *
 * @ORM\Table(name="Favorite", uniqueConstraints={@ORM\UniqueConstraint(name="Favorite_ID_uindex", columns={"ID"})})
 * @ORM\Entity
 */
class Favorite
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="fk_user_id", type="integer", nullable=false)
     */
    private $fkUserId;

    /**
     * @var int
     *
     * @ORM\Column(name="fk_bar_id", type="integer", nullable=false)
     */
    private $fkBarId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true, options={"default"="1"})
     */
    private $isActive = '1';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFkUserId(): ?int
    {
        return $this->fkUserId;
    }

    public function setFkUserId(int $fkUserId): self
    {
        $this->fkUserId = $fkUserId;

        return $this;
    }

    public function getFkBarId(): ?int
    {
        return $this->fkBarId;
    }

    public function setFkBarId(int $fkBarId): self
    {
        $this->fkBarId = $fkBarId;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

}
