<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\User;
/**
 * Owner
 *
 * @ORM\Table(name="Owner")
 * @ORM\Entity
 */
class Owner
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="identity_proof", type="string", length=255, nullable=true)
     */
    private $identityProof;

    /**
     * @var User
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_id_user", referencedColumnName="ID")
     * })
     */
    private $fkIdUser;

    public function getIdentityProof(): ?string
    {
        return $this->identityProof;
    }

    public function setIdentityProof(?string $identityProof): self
    {
        $this->identityProof = $identityProof;

        return $this;
    }

    public function getFkIdUser(): ?User
    {
        return $this->fkIdUser;
    }

    public function setFkIdUser(?User $fkIdUser): self
    {
        $this->fkIdUser = $fkIdUser;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(){
        return "{$this->fkIdUser->getFirstName()} {$this->fkIdUser->getLastName()}";
    }

}
