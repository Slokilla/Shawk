<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PendingUser
 *
 * @ORM\Table(name="pending_user", uniqueConstraints={@ORM\UniqueConstraint(name="pending_user_ID_uindex", columns={"ID"})}, indexes={@ORM\Index(name="pending_user_User_ID_fk", columns={"pending_user"})})
 * @ORM\Entity
 */
class PendingUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pending_user", referencedColumnName="ID")
     * })
     */
    private $pendingUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getPendingUser(): ?User
    {
        return $this->pendingUser;
    }

    public function setPendingUser(?User $pendingUser): self
    {
        $this->pendingUser = $pendingUser;

        return $this;
    }


}
