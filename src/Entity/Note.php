<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Note
 *
 * @ORM\Table(name="Note", uniqueConstraints={@ORM\UniqueConstraint(name="Note_ID_uindex", columns={"ID"})}, indexes={@ORM\Index(name="Note_User_ID_fk", columns={"fk_user_id"}), @ORM\Index(name="Note_Bar_ID_fk", columns={"fk_bar_id"})})
 * @ORM\Entity
 */
class Note
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="note_value", type="integer", nullable=false)
     */
    private $noteValue;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var Bar
     *
     * @ORM\ManyToOne(targetEntity="Bar")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_bar_id", referencedColumnName="ID")
     * })
     */
    private $fkBar;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_user_id", referencedColumnName="ID")
     * })
     */
    private $fkUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNoteValue(): ?int
    {
        return $this->noteValue;
    }

    public function setNoteValue(int $noteValue): self
    {
        $this->noteValue = $noteValue;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getFkBar(): ?Bar
    {
        return $this->fkBar;
    }

    public function setFkBar(?Bar $fkBar): self
    {
        $this->fkBar = $fkBar;

        return $this;
    }

    public function getFkUser(): ?User
    {
        return $this->fkUser;
    }

    public function setFkUser(?User $fkUser): self
    {
        $this->fkUser = $fkUser;

        return $this;
    }


}
