<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Links
 *
 * @ORM\Table(name="Links", uniqueConstraints={@ORM\UniqueConstraint(name="Links_ID_uindex", columns={"ID"})}, indexes={@ORM\Index(name="Links_Bar_ID_fk", columns={"fk_bar_id"})})
 * @ORM\Entity
 */
class Links
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=false)
     */
    private $link;

    /**
     * @var Bar
     *
     * @ORM\ManyToOne(targetEntity="Bar")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_bar_id", referencedColumnName="ID")
     * })
     */
    private $fkBar;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getFkBar(): ?Bar
    {
        return $this->fkBar;
    }

    public function setFkBar(?Bar $fkBar): self
    {
        $this->fkBar = $fkBar;

        return $this;
    }


}
