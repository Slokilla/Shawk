<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="User", uniqueConstraints={@ORM\UniqueConstraint(name="User_ID_uindex", columns={"ID"})}, indexes={@ORM\Index(name="User_File_ID_fk", columns={"fk_profil_picture"}), @ORM\Index(name="User_Account_type_ID_fk", columns={"fk_account_type_id"})})
 * @ORM\Entity
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pass", type="string", length=255, nullable=true)
     */
    private $pass;

    /**
     * @var int|null
     *
     * @ORM\Column(name="birth_date", type="integer", nullable=true)
     */
    private $birthDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="signup_date", type="integer", nullable=true)
     */
    private $signupDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="last_login", type="integer", nullable=true)
     */
    private $lastLogin;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="File")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_profil_picture", referencedColumnName="ID")
     * })
     */
    private $fkProfilPicture;

    /**
     * @var AccountType
     *
     * @ORM\ManyToOne(targetEntity="AccountType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_account_type_id", referencedColumnName="ID")
     * })
     */
    private $fkAccountType;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->email;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->pass;
    }

    public function setPass(?string $pass): self
    {
        $this->pass = $pass;

        return $this;
    }

    public function getBirthDate(): ?int
    {
        return $this->birthDate;
    }

    public function setBirthDate(?int $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getSignupDate(): ?int
    {
        return $this->signupDate;
    }

    public function setSignupDate(?int $signupDate): self
    {
        $this->signupDate = $signupDate;

        return $this;
    }

    public function getLastLogin(): ?int
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?int $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getFkProfilPicture(): ?File
    {
        return $this->fkProfilPicture;
    }

    public function setFkProfilPicture(?File $fkProfilPicture): self
    {
        $this->fkProfilPicture = $fkProfilPicture;

        return $this;
    }

    public function getFkAccountType(): ?AccountType
    {
        return $this->fkAccountType;
    }

    public function setFkAccountType(?AccountType $fkAccountType): self
    {
        $this->fkAccountType = $fkAccountType;

        return $this;
    }

    public function setAll(User $user)
    {
        $this->firstName = $user->firstName;
        $this->lastName = $user->lastName;
        $this->email = $user->email;
        $this->birthDate = $user->birthDate;
        $this->fkProfilPicture = $user->fkProfilPicture;

    }


    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return array($this->getFkAccountType()->getRoleName(),);
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // we don't do it here
        return null;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
