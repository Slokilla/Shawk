<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\BarsEvent;

/**
 * Event
 *
 * @ORM\Table(name="Event", uniqueConstraints={@ORM\UniqueConstraint(name="Event_ID_uindex", columns={"ID"})}, indexes={@ORM\Index(name="Event_File_image_event_fk", columns={"fk_image_affiche"}), @ORM\Index(name="Event_Bar_ID_fk", columns={"fk_bar_id"})})
 * @ORM\Entity
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ID;

    /**
     * @var string
     *
     * @ORM\Column(name="event_name", type="string", length=255, nullable=false)
     */
    private $eventName;

    /**
     * @var int
     *
     * @ORM\Column(name="publication_date", type="integer", nullable=false)
     */
    private $publicationDate;

    /**
     * @var int
     *
     * @ORM\Column(name="event_date", type="integer", nullable=false)
     */
    private $eventDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="information", type="string", length=255, nullable=true)
     */
    private $information;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="File")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_image_affiche", referencedColumnName="ID")
     * })
     */
    private $fkImageAffiche;

    /**
     * @var int
     * @ORM\Column(name="fk_bar_id", type="integer", nullable=true)
     */
    private $fkBar;

    public function getID(): ?int
    {
        return $this->ID;
    }

    public function getEventName(): ?string
    {
        return $this->eventName;
    }

    public function setEventName(string $eventName): self
    {
        $this->eventName = $eventName;

        return $this;
    }

    public function getPublicationDate(): ?\DateTimeInterface
    {
        $date = date('Y-m-d H:i:s', ($this->publicationDate));
        try {
            return new \DateTime($date);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function setPublicationDate(): self
    {
        $this->publicationDate = time();

        return $this;
    }

    public function getEventDate(): ?\DateTimeInterface
    {
        $date = date('Y-m-d H:i:s', ($this->eventDate));
        try {
            return new \DateTime($date);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function setEventDate(\DateTime $date): self
    {
        $this->eventDate = $date->getTimestamp();;

        return $this;
    }

    public function getInformation(): ?string
    {
        return $this->information;
    }

    public function setInformation(?string $information): self
    {
        $this->information = $information;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getFkImageAffiche(): ?File
    {
        return $this->fkImageAffiche;
    }

    public function getAfficheFilepath(): ?string
    {
        if ($this->fkImageAffiche)
            return $this->fkImageAffiche->getFilepath();
        return null;
    }

    public function setFkImageAffiche(?File $fkImageAffiche): self
    {
        $this->fkImageAffiche = $fkImageAffiche;

        return $this;
    }

    public function getFkBar(): ?int
    {
        return $this->fkBar;
    }

    public function setFkBar(?int $id): self
    {
        $this->fkBar = $id;
        return $this;
    }
}
